package display;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.image.ImageView;

/**
 * Display all characters and his dependencies (like bullets)
 */
public class DisplayCharacter {

    /**
     * Display a character
     * @param iV Skin character
     * @param LRatio The width ration
     * @param HRatio The height ratio
     * @param hGX The X location (width)
     * @param hGY The Y location (height)
     * @param background The background image
     */
    public static void characterDisplay(ImageView iV, double LRatio, double HRatio, SimpleDoubleProperty hGX, SimpleDoubleProperty hGY, ImageView background) {
        iV.fitHeightProperty().bind(background.fitHeightProperty().multiply(HRatio));
        iV.fitWidthProperty().bind(background.fitWidthProperty().multiply(LRatio));
        iV.layoutXProperty().bind(background.fitWidthProperty().multiply(hGX));
        iV.layoutYProperty().bind(background.fitHeightProperty().multiply(hGY));
    }

    /**
     * Display the bullet when a character shoot
     * @param b Bullet skin
     * @param background The background image
     * @param hGX The X location
     * @param hGY The Y location
     */
    public static void displayShoot(ImageView b, ImageView background, SimpleDoubleProperty hGX, SimpleDoubleProperty hGY) {
        b.layoutXProperty().bind(background.fitWidthProperty().multiply(hGX));
        b.layoutYProperty().bind(background.fitHeightProperty().multiply(hGY));
    }
}
