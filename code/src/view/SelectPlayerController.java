package view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;
import launcher.Game;
import model.characters.CharacterManager;
import model.characters.PlayerCharacter;
import model.weapons.Gun;

import java.io.IOException;
import java.util.*;


public class SelectPlayerController {

    @FXML
    private Button btnBack;
    @FXML
    private Button btnStart1;
    @FXML
    private void btnBack(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage)btnBack.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/home.fxml"));
        stage.setScene(new Scene(root, 1000, 800));
        stage.getScene().getStylesheets().add("/style/stylesheet.css");
        stage.setFullScreen(true);
        stage.setFullScreenExitHint("");
        stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
    }

    @FXML
    private void btnStart1(ActionEvent actionEvent) {
        CharacterManager characterManage = new CharacterManager();

        //PlayerCharacter pc = characterManage.getRandomPlayerCharacter();
        PlayerCharacter pc = new PlayerCharacter("neo", 15, new Gun("revolver", 1, 10, new ImageView(new Image("/texture/skin/balle.png"))), null, new ImageView(new Image("/texture/skin/character/rambro.png")));
        Game game = new Game(pc, null);
        Stage s = (Stage)btnStart1.getScene().getWindow();
        game.gameLoader(s);
    }
}

