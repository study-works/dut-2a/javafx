package view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import launcher.Main;

import java.io.IOException;

public class HomeController {
    @FXML
    private GridPane grid;

    @FXML
    private Button btnQuit;

    @FXML
    private Button btnContribs;

    @FXML
    private Button btnStartGame;

    @FXML
    private void initialize() {
        BackgroundImage myBI= new BackgroundImage(new Image("/Image/broforceBackground.jpg"),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                BackgroundSize.DEFAULT);
        grid.setBackground(new Background(myBI));
        //grid.setStyle("-fx-background-image: url('/Image/broforceBackground.jpg')");
    }

    @FXML
    private void btnStartGame(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage)btnStartGame.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/SelectPlayer.fxml"));
        stage.setScene(new Scene(root, 1000, 800));
        stage.getScene().getStylesheets().add("/style/stylesheet.css");
        stage.setFullScreen(true);
        stage.setFullScreenExitHint("");
        stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
    }

    @FXML
    private void btnContribs() throws IOException {
        Stage stg = new Stage();
        Scene s = new Scene(FXMLLoader.load(getClass().getResource("/fxml/Contributors.fxml")));
        stg.setScene(s);
        stg.initOwner(Main.getPrimaryStage());
        stg.getScene().getStylesheets().add("/style/stylesheet.css");
        stg.showAndWait();
    }

    @FXML
    private void btnQuit(ActionEvent actionEvent) {
        ((Stage)btnQuit.getScene().getWindow()).close();
    }
}
