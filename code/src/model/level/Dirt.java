package model.level;


import javafx.scene.image.ImageView;

/**
 * The dirt block, the block by default. He is destructible
 * Extends Block
 * @see Block
 */
public class Dirt extends Block {
    /**
     * Construct the dirt block, with the name "dirt"
     * @param x The X location
     * @param y The Y location
     */
    public Dirt(double x, double y, ImageView blockSkin) {
        super("Dirt", x, y, true, blockSkin);
    }
}
