package model.level;

import javafx.scene.image.ImageView;

/**
 * The general bloc class (for levels)
 */
public abstract class Block {
    /**
     * Name of the bloc
     */
    private String name;
    /**
     * The X location
     */
    private double coordonateX;
    /**
     * The Y location
     */
    private double coordonateY;
    /**
     * Indicates whether the block is destructible or not
     */
    private boolean destructible;
    private ImageView blockSkin;
    /**
     * Construct the bloc
     * @param name The name of the bloc
     * @param x The X coordonate
     * @param y The Y coordonate
     * @param destructible Indicate if the block is destructible or not
     * @param blockSkin Skin of the block
     */
    public Block(String name, double x, double y, boolean destructible, ImageView blockSkin) {
        this.name = name;
        coordonateX = x;
        coordonateY = y;
        this.destructible = destructible;
        this.blockSkin = blockSkin;
    }

    public String getName() { return name; }
    private void setName(String name) { this.name = name; }

    public double getCoordonateX() { return coordonateX; }
    public void setCoordonateX(double coordonateX) { this.coordonateX = coordonateX; }

    public double getCoordonateY() { return coordonateY; }
    public void setCoordonateY(double coordonateY) { this.coordonateY = coordonateY; }

    public boolean getDestructible() { return destructible; }
    private void setDestructible(boolean destructible) { this.destructible = destructible; }

    public ImageView getBlockSkin() { return blockSkin; }
    public void setBlockSkin(ImageView blockSkin) { this.blockSkin = blockSkin; }
}
