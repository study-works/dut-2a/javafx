package model.contributors;

/**
 * Class Stub for the contributor
 */
public class Stub {

    public static Contributors generateContributors() {
        Contributors contributors = new Contributors();
        contributors.addcontributor(new Contributor("BRUGIÈRE", "Gaëthan", "8"));
        contributors.addcontributor(new Contributor("LAGORSSE", "Romain", "7"));

        return contributors;
    }
}
