package model.weapons;

import javafx.scene.image.ImageView;

/**
 * The gun weapon class
 * Extends Weapon
 * @see Weapon
 */
public class Gun extends Weapon {
    /**
     * Construct the gun
     * @param name The name of the gun
     * @param damage The damage caused by the gun
     * @param range The range of the gun
     * @param attackTexture The attack texture of the gun (image)
     */
    public Gun(String name, int damage, int range, ImageView attackTexture) {
        super(name, damage, range, attackTexture);
    }
}
