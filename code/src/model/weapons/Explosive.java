package model.weapons;

import javafx.scene.image.ImageView;

/**
 * The explosive weapon class
 * Extends Weapon
 * @see Weapon
 */
public class Explosive extends Weapon {
    /**
     * Construct the explosive
     * @param name The name of the explosive
     * @param damage The damage caused by the explosive
     * @param range The range of the explosive
     * @param attackTexture The attack texture of the explosive (image)
     */
    public Explosive(String name, int damage, int range, ImageView attackTexture) {
        super(name, damage, range, attackTexture);
    }
}
