package model.weapons;

import javafx.scene.image.ImageView;

/**
 * The blade weapon class
 * Extends Weapon
 * @see Weapon
 */
public class Blade extends Weapon {
    /**
     * Construct the blade
     * @param name The name of the blade
     * @param damage The damage caused by the blade
     * @param range The range of the blade
     * @param attackTexture The attack texture of the blade (image)
     */
    public Blade(String name, int damage, int range, ImageView attackTexture) {
        super(name, damage, range, attackTexture);
    }
}
