package model.weapons;

import display.DisplayCharacter;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Group;
import model.characters.Character;
import javafx.scene.image.ImageView;
import model.characters.CharacterMover;
import model.characters.Enemy;

/**
 * Manages the shooting of a weapon
 */
public class WeaponShoot {

    private static final double RIGHT_RANGE = 1d;
    private static final double LEFT_RANGE = 0d;
    private static final double SHOOT_SPEED = 0.01d;
    private static final double ATTACK_RANGE = 0.015d;
    private static final double L_RATIO = 0.046875d;
    private static final double H_RATIO = 0.166666d;

    public WeaponShoot() { }

    /**
     * Make the weapon shot
     * @param character the character who's shoting
     * @param root the group of the scene
     * @param enemy the enemy for the hitbox
     */
    public static void shoot(Character character, Group root, Enemy enemy) {
        if (character.getDirectionView().equals("right")) {
            character.getWeapon().getX().set(character.getWeapon().getX().get() + SHOOT_SPEED);
            if(character.getWeapon().getX().get() > RIGHT_RANGE) {
                character.getWeapon().setShoot(false);
                root.getChildren().remove(character.getWeapon().getAttackTexture());
            }
            if (character.getX().get() <= enemy.getX().get()) {
                if (character.getWeapon().getX().get() >= enemy.getX().get()) {
                    if (enemy.getY().get() + H_RATIO <= character.getWeapon().getY().get() || character.getWeapon().getY().get() >= enemy.getY().get()) {
                        character.getWeapon().setShoot(false);
                        root.getChildren().remove(character.getWeapon().getAttackTexture());
                        enemy.setLife(enemy.getLife() - character.getWeapon().getDamage());
                    }
                }
            }
        }
        if (character.getDirectionView().equals("left")) {
            character.getWeapon().getX().set(character.getWeapon().getX().get() - SHOOT_SPEED);
            if(character.getWeapon().getX().get() < LEFT_RANGE) {
                character.getWeapon().setShoot(false);
                root.getChildren().remove(character.getWeapon().getAttackTexture());
            }
            if (character.getX().get() >= enemy.getX().get()) {
                if (character.getWeapon().getX().get() <= enemy.getX().get() + L_RATIO) {
                    if (enemy.getY().get() + H_RATIO <= character.getWeapon().getY().get() || character.getWeapon().getY().get() >= enemy.getY().get()) {
                        character.getWeapon().setShoot(false);
                        root.getChildren().remove(character.getWeapon().getAttackTexture());
                        enemy.setLife(enemy.getLife() - character.getWeapon().getDamage());
                    }
                }
            }
        }

    }

    /**
     * create the shoot
     * @param character character who's shoting
     * @param background background image
     * @param root group of the scene
     */
    public static void createShoot(Character character, ImageView background, Group root) {
        character.getWeapon().setX(new SimpleDoubleProperty(character.getX().get() + 0.02d));
        character.getWeapon().setY(new SimpleDoubleProperty(character.getY().get() + 0.1d));
        DisplayCharacter.displayShoot(character.getWeapon().getAttackTexture(), background, character.getWeapon().getX(), character.getWeapon().getY());
        root.getChildren().add(character.getWeapon().getAttackTexture());
    }

    /**
     * make the enemy attack and aggro the player
     * @param character the player
     * @param enemy the enemy who attack
     */
    public static void enemyAttack(Character character, Enemy enemy) {
        CharacterMover.enemyMove(enemy);
        if(character.getY().get() == enemy.getY().get()) {
            if (character.getX().get() + L_RATIO <= enemy.getX().get()) {
                enemy.setDirectionView("left");
                enemy.setIsAttacking(false);
            }
            if (character.getX().get() >= enemy.getX().get() + L_RATIO) {
                enemy.setDirectionView("right");
                enemy.setIsAttacking(false);
            }
            if(Math.abs(character.getX().get() - enemy.getX().get()) <= ATTACK_RANGE && !enemy.getIsAttacking()) {
                character.setLife(character.getLife()-3);
                enemy.setIsAttacking(true);
            }
        }
    }
 }
