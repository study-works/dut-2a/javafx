package model.characters;

import javafx.scene.image.ImageView;
import model.weapons.Weapon;

/**
 * The playable character class
 * Extends Character
 * @see Character
 */
public class PlayerCharacter extends Character implements Comparable<PlayerCharacter> {
    /**
     * A special weapon of the character
     */
    private Weapon specialWeapon;
    /**
     * Indicates whether the character is jumping or not
     * By default, he don't jump
     */
    private boolean jump = false;
//    private boolean isGrounded = false;

    /**
     * Construct a playable character
     * @param name The name of the character
     * @param life The life of the character
     * @param weapon The weapon of the character
     * @param specialWeapon The special weapon of the character
     * @param skin The skin of the character (image)
     */
    public PlayerCharacter(String name, int life, Weapon weapon, Weapon specialWeapon, ImageView skin) {
        super(name, life, weapon, skin);
        this.specialWeapon = specialWeapon;
    }

    public Weapon getSpecialWeapon() { return specialWeapon; }
    private void setSpecialWeapon(Weapon specialWeapon) { this.specialWeapon = specialWeapon; }

    public boolean getJump() { return jump; }
    public void setJump(boolean jump) { this.jump = jump; }

//    public boolean isGrounded() {return isGrounded;}
//    public void setGrounded(boolean grounded) { isGrounded = grounded; }

    @Override
    public boolean equals(Object obj) {
        PlayerCharacter playerCharacter = (PlayerCharacter) obj;
        return this == playerCharacter;
//        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return (super.getName().hashCode() * 42);
    }

    @Override
    public int compareTo(PlayerCharacter playerCharacter) {
        return 0;
    }
}
