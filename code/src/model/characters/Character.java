package model.characters;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.image.ImageView;
import model.weapons.Weapon;


/**
 * The general character class
 */
public abstract class Character {
    /**
     * Name of the character
     */
    private String name;
    /**
     * Life of the character
     */
    private int life;
    /**
     * Weapon of the character
     * @see Weapon
     */
    private Weapon weapon;
    /**
     * The skin of the character
     */
    private ImageView skin;
    /**
     * The direcion view of the character
     * Right by default
     */
    private String directionView = "right";
    /**
     * Indicates whether the character is walking or not
     * By default, he don't walk
     */
    private boolean walk = false;
    /**
     * The X location
     */
    private SimpleDoubleProperty x;
    /**
     * The Y location
     */
    private SimpleDoubleProperty y;

    /**
     * Construct a character
     * @param name The name
     * @param life The maximum life
     * @param weapon The weapon
     * @param skin The skin (image)
     */
    public Character(String name, int life, Weapon weapon, ImageView skin) {
        this.name = name;
        this.life = life;
        this.weapon = weapon;
        this.skin = skin;
    }

    public String getName() { return name; }
    private void setName(String name) { this.name = name; }

    public int getLife() { return life; }
    public void setLife(int life) { this.life = life; }

    public Weapon getWeapon() { return weapon; }
    private void setWeapon(Weapon weapon) { this.weapon = weapon; }

    public ImageView getSkin() { return skin; }
    private void setSkin(ImageView skin) { skin = skin; }

    public SimpleDoubleProperty getX() { return  x; }
    public void setX(SimpleDoubleProperty X) {this.x = X;}

    public SimpleDoubleProperty getY() { return  y; }
    public void setY(SimpleDoubleProperty Y) {this.y = Y;}

    public String getDirectionView() { return directionView; }
    public void setDirectionView(String directionView) { this.directionView = directionView; }

    public boolean getWalk() { return walk; }
    public void setWalk(boolean walk) { this.walk = walk; }
}
