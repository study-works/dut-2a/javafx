package launcher;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;


public class Main extends Application {

    /**
     * Variable contenant le nom du jeu
     */
    private static final String GAME_TITLE = "BroForce";

    private static Stage primaryStage;

    /**
     * Initializes the first window
     * @param stage First Stage
     * @throws Exception Exceptions if problem with the stages
     */
    @Override
    public void start(Stage stage) throws Exception{
        primaryStage = stage;
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/home.fxml"));
        primaryStage.setTitle(GAME_TITLE);
        primaryStage.setScene(new Scene(root, 1000, 800));
        primaryStage.getScene().getStylesheets().add("/style/stylesheet.css");
        primaryStage.setFullScreen(true);
        primaryStage.setFullScreenExitHint("");
        primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    public static Stage getPrimaryStage() {
        return primaryStage;
    }
}
