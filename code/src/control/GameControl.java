package control;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;
import model.characters.PlayerCharacter;
import model.weapons.WeaponShoot;

import java.io.IOException;

/**
 * Controller of the game
 */
public class GameControl {
    public GameControl() {

    }

    /**
     * Controls the playable character
     * @param scene The scene in which the character finds himself
     * @param character The player character
     * @param background The background image
     * @param root A group to a stage
     * @param s Parent stage
     * @param loop The loop of the game
     */
    public static void control(Scene scene, PlayerCharacter character, ImageView background, Group root, Stage s, AnimationTimer loop) {
        scene.setOnKeyPressed(e -> {
            switch(e.getCode()){
                case LEFT :
                    character.setDirectionView("left");
                    character.setWalk(true);
                    break;
                case RIGHT :
                    character.setDirectionView("right");
                    character.setWalk(true);
                    break;
                case UP :
                    if(!character.getJump()) {
                        character.setJump(true);
                    }
                    break;
                case ENTER :
                    if (!character.getWeapon().getShoot()) {
                        WeaponShoot.createShoot(character, background, root);
                        character.getWeapon().setShoot(true);
                    }
                    break;
                case ESCAPE:
                    try {
                        loop.stop();
                        Parent root1 = FXMLLoader.load(GameControl.class.getResource("/fxml/home.fxml"));
                        s.setScene(new Scene(root1, 1000, 800));
                        s.getScene().getStylesheets().add("/style/stylesheet.css");
                        s.setFullScreen(true);
                        s.setFullScreenExitHint("");
                        s.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    break;
                default:
                    break;
            }
        });

        scene.setOnKeyReleased(e -> {
            switch(e.getCode()){
                case LEFT :
                case RIGHT :
                    character.setWalk(false);
                    break;
                default:
                    break;
            }
        });
    }
}
